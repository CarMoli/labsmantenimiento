const express = require('express');
const controller = require('../controlador/cmain');
const router = express.Router();

const cmain = require('../controlador/cmain');

router.get('/',cmain.bitnueva);

router.get('/bitacora/nueva',cmain.bitnueva);
router.post('/bitacora/nueva/add',cmain.bitadd);
router.get('/bitacora/continuar',cmain.bitcontinuar);
router.get('/bitacora/continuar/:id',cmain.biteditar);
router.post('/bitacora/continuar/update/:id',cmain.bitupd);
router.get('/bitacora/pdf/:id',cmain.bitpdf);

router.get('/busqueda',cmain.busqueda);
router.post('/busqueda/fecha',cmain.busfecha);
router.post('/busqueda/equipo',cmain.busequipo);
router.post('/busqueda/empresa',cmain.busempresa);
router.post('/busqueda/trabajador',cmain.bustrabajador);
router.post('/busqueda/tecnico',cmain.bustecnico);

router.get('/administrador/bitacora/del/:id',cmain.admbitacoradel);
router.get('/administrador/bitacora/update/:id',cmain.admbitacoraedi);
router.post('/administrador/bitacora/update/:id',cmain.admbitacoraupd);
router.get('/administrador/bitacora/pdf',cmain.pdf_bitacora);

router.post('/administrador/empresa/add',cmain.admempresaadd);
router.post('/administrador/empresa/update/:id',cmain.admempresaupd);
router.get('/administrador/empresa/pdf',cmain.pdf_empresa);

router.post('/administrador/equipo/add',cmain.admequipoadd);
router.post('/administrador/equipo/update/:id',cmain.admequipoupd);
router.get('/administrador/equipo/pdf',cmain.pdf_equipo);

router.post('/administrador/trabajador/add',cmain.admtrabajadoradd);
router.post('/administrador/trabajador/update/:id',cmain.admtrabajadorupd);
router.get('/administrador/trabajador/pdf',cmain.pdf_trabajador);

router.post('/administrador/tecnico/add',cmain.admtecnicoadd);
router.post('/administrador/tecnico/update/:id',cmain.admtecnicoupd);
router.get('/administrador/tecnico/pdf',cmain.pdf_tecnico);

module.exports = router;