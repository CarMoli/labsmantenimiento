const { render } = require('ejs');

const controller = {};

controller.main = (req,res) => {
    res.render('main');
};

controller.bitnueva = (req,res) => {
    req.getConnection((err, con) => {
        con.query('SELECT * FROM trabajador',(err, datos_trabajador) => {
            con.query('SELECT * FROM tecnico INNER JOIN empresa on idempresa = ID_empresa',(err, datos_tecnico) => {
                con.query('SELECT * FROM equipo',(err, datos_equipo) => {
                    con.query('SELECT * FROM empresa',(err,datos_empresa) => {
                        con.query('SELECT DISTINCT marca FROM equipo',(err, datos_marca) => {
                            con.query('SELECT DISTINCT marca,modelo FROM equipo',(err, datos_modelo) => {
                                con.query('SELECT ID_MANTENIMIENTO FROM mantenimiento ORDER BY ID_MANTENIMIENTO DESC LIMIT 1',(err,folio) => {
                                    res.render('bitacora/nueva/nueva',{
                                        bdtrabajador: datos_trabajador, bdtecnico: datos_tecnico, bdequipo : datos_equipo, bdempresa : datos_empresa, fol: folio[0], bdmarca: datos_marca, bdmodelo:datos_modelo
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}


controller.bitadd = (req,res) =>{
    const data = req.body;

    req.getConnection((err, con) => {
        con.query('INSERT INTO mantenimiento set ?',[data], (err, row) => {
            
            console.log('mantenimiento agregado');
            res.redirect('/bitacora/nueva');
        })
    });
}

controller.bitcontinuar = (req,res) => {
    req.getConnection((err, con) => {
        con.query('SELECT * FROM `mantenimiento` INNER JOIN trabajador on idtrabajador = ID_trabajador INNER JOIN tecnico on idtecnico = Id_tecnico INNER JOIN equipo on idequipo = Id_equipo INNER JOIN empresa on idempresa = ID_empresa ORDER BY ID_MANTENIMIENTO',(err, datos) => {
            if(err){
            res.json(err);
            }
            res.render('bitacora/continuar/continuar',{
                data: datos
            });
        });
    });
}

controller.biteditar = (req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('SELECT * FROM `mantenimiento` INNER JOIN trabajador on idtrabajador = ID_trabajador INNER JOIN tecnico on idtecnico = Id_tecnico INNER JOIN equipo on idequipo = Id_equipo INNER JOIN empresa on idempresa = ID_empresa WHERE ID_MANTENIMIENTO = ?',[id], (err, row) => {
            res.render('bitacora/continuar/editar',{
                data: row[0]
            });
        });
    });
}           
           

controller.bitupd = (req, res) => {
    const { id } = req.params;
    const newdata = req.body;

    req.getConnection((err, con) => {
        con.query('UPDATE mantenimiento set ?  WHERE ID_MANTENIMIENTO = ?',[newdata,id], (err, row) => {
            con.query('UPDATE mantenimiento SET estado=1 WHERE ID_MANTENIMIENTO = ?',[id], (err, row) => {
                res.redirect('/bitacora/continuar');
            });
        });
    });
}

controller.bitpdf = (req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('SELECT * FROM `mantenimiento` INNER JOIN trabajador on idtrabajador = ID_trabajador INNER JOIN tecnico on idtecnico = Id_tecnico INNER JOIN equipo on idequipo = Id_equipo INNER JOIN empresa on idempresa = ID_empresa WHERE ID_MANTENIMIENTO = ?',[id], (err, row) => {
            res.render('bitacora/pdf_bitacora',{data : row});
        })
    });
};

controller.busqueda = (req,res) => {
    req.getConnection((err, con) => {
        con.query('SELECT * FROM `mantenimiento` INNER JOIN trabajador on idtrabajador = ID_trabajador INNER JOIN tecnico on idtecnico = Id_tecnico INNER JOIN equipo on idequipo = Id_equipo INNER JOIN empresa on idempresa = ID_empresa ORDER BY ID_MANTENIMIENTO',(err, datos) => {
            con.query('SELECT * FROM trabajador',(err, datos_trabajador) => {
                con.query('SELECT * FROM tecnico',(err, datos_tecnico) => {
                    con.query('SELECT * FROM equipo',(err, datos_equipo) => {
                        con.query('SELECT * FROM empresa',(err,datos_empresa) => {
                            if(err){
                            res.json(err);
                            }
                            res.render('busqueda/busqueda',{
                                data: datos, bdtrabajador: datos_trabajador, bdtecnico: datos_tecnico, bdequipo : datos_equipo, bdempresa : datos_empresa
                            });
                        });
                    });
                });
            });
        });
    });
};

controller.busfecha = (req,res) => {
    const data = req.body;
    console.log(data);
    req.getConnection((err, con) => {
        con.query('SELECT * FROM mantenimiento WHERE inicio_f = ? ',[data.inicio_f],(err, datos_bitacora) => {
            con.query('SELECT * FROM trabajador',(err, datos_trabajador) => {
                con.query('SELECT * FROM tecnico',(err, datos_tecnico) => {
                    con.query('SELECT * FROM equipo',(err, datos_equipo) => {
                        con.query('SELECT * FROM empresa',(err,datos_empresa) => {
                            res.render('busqueda/busqueda',{
                                bdbitacora: datos_bitacora, bdtrabajador: datos_trabajador, bdtecnico: datos_tecnico, bdequipo : datos_equipo, bdempresa : datos_empresa
                            });
                        });
                    });
                });
            });
        });
    });
};

controller.busequipo = (req,res) => {
    const data = req.body;
    req.getConnection((err, con) => {
        con.query('SELECT * FROM `mantenimiento` INNER JOIN trabajador on idtrabajador = ID_trabajador INNER JOIN tecnico on idtecnico = Id_tecnico INNER JOIN equipo on idequipo = Id_equipo INNER JOIN empresa on idempresa = ID_empresa where idequipo = ? ORDER BY ID_MANTENIMIENTO',[data.id_equipo],(err, datos) => {
            con.query('SELECT * FROM trabajador',(err, datos_trabajador) => {
                con.query('SELECT * FROM tecnico',(err, datos_tecnico) => {
                    con.query('SELECT * FROM equipo',(err, datos_equipo) => {
                        con.query('SELECT * FROM empresa',(err,datos_empresa) => {
                            if(err){
                            res.json(err);
                            }
                            res.render('busqueda/busqueda',{
                                data: datos, bdtrabajador: datos_trabajador, bdtecnico: datos_tecnico, bdequipo : datos_equipo, bdempresa : datos_empresa
                            });
                        });
                    });
                });
            });
        });
    });
};

controller.bustrabajador = (req,res) => {
    const data = req.body;
    req.getConnection((err, con) => {
        con.query('SELECT * FROM `mantenimiento` INNER JOIN trabajador on idtrabajador = ID_trabajador INNER JOIN tecnico on idtecnico = Id_tecnico INNER JOIN equipo on idequipo = Id_equipo INNER JOIN empresa on idempresa = ID_empresa where idtrabajador = ? ORDER BY ID_MANTENIMIENTO',[data.id_trabajador],(err, datos) => {
            con.query('SELECT * FROM trabajador',(err, datos_trabajador) => {
                con.query('SELECT * FROM tecnico',(err, datos_tecnico) => {
                    con.query('SELECT * FROM equipo',(err, datos_equipo) => {
                        con.query('SELECT * FROM empresa',(err,datos_empresa) => {
                            if(err){
                            res.json(err);
                            }
                            res.render('busqueda/busqueda',{
                                data: datos, bdtrabajador: datos_trabajador, bdtecnico: datos_tecnico, bdequipo : datos_equipo, bdempresa : datos_empresa
                            });
                        });
                    });
                });
            });
        });
    });
};

controller.busempresa = (req,res) => {
    const data = req.body;
    req.getConnection((err, con) => {
        con.query('SELECT * FROM `mantenimiento` INNER JOIN trabajador on idtrabajador = ID_trabajador INNER JOIN tecnico on idtecnico = Id_tecnico INNER JOIN equipo on idequipo = Id_equipo INNER JOIN empresa on idempresa = ID_empresa where idempresa = ? ORDER BY ID_MANTENIMIENTO',[data.id_empresa],(err, datos) => {
            con.query('SELECT * FROM trabajador',(err, datos_trabajador) => {
                con.query('SELECT * FROM tecnico',(err, datos_tecnico) => {
                    con.query('SELECT * FROM equipo',(err, datos_equipo) => {
                        con.query('SELECT * FROM empresa',(err,datos_empresa) => {
                            if(err){
                            res.json(err);
                            }
                            res.render('busqueda/busqueda',{
                                data: datos, bdtrabajador: datos_trabajador, bdtecnico: datos_tecnico, bdequipo : datos_equipo, bdempresa : datos_empresa
                            });
                        });
                    });
                });
            });
        });
    });
};

controller.bustecnico = (req,res) => {
    const data = req.body;
    req.getConnection((err, con) => {
        con.query('SELECT * FROM `mantenimiento` INNER JOIN trabajador on idtrabajador = ID_trabajador INNER JOIN tecnico on idtecnico = Id_tecnico INNER JOIN equipo on idequipo = Id_equipo INNER JOIN empresa on idempresa = ID_empresa where idtecnico = ? ORDER BY ID_MANTENIMIENTO',[data.id_tecnico],(err, datos) => {
            con.query('SELECT * FROM trabajador',(err, datos_trabajador) => {
                con.query('SELECT * FROM tecnico',(err, datos_tecnico) => {
                    con.query('SELECT * FROM equipo',(err, datos_equipo) => {
                        con.query('SELECT * FROM empresa',(err,datos_empresa) => {
                            if(err){
                            res.json(err);
                            }
                            res.render('busqueda/busqueda',{
                                data: datos, bdtrabajador: datos_trabajador, bdtecnico: datos_tecnico, bdequipo : datos_equipo, bdempresa : datos_empresa
                            });
                        });
                    });
                });
            });
        });
    });
};

controller.admbitacora = (req,res) => {
    req.getConnection((err, con) => {
        con.query('SELECT * FROM `mantenimiento` INNER JOIN trabajador on idtrabajador = ID_trabajador INNER JOIN tecnico on idtecnico = Id_tecnico INNER JOIN equipo on idequipo = Id_equipo INNER JOIN empresa on idempresa = ID_empresa ORDER BY ID_MANTENIMIENTO',(err, datos) => {
            if(err){
                res.json(err);
            }
            res.render('administrador/bitacora/bitacora',{
                data: datos
            });
        });
    });
};

controller.admbitacoradel = (req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('DELETE FROM mantenimiento WHERE ID_MANTENIMIENTO = ?',[id], (err, row) => {
            console.log('mantenimiento eliminado');
            res.redirect('/administrador/bitacora');
        })
    });
};

controller.admbitacoraedi = (req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('SELECT * FROM trabajador',(err, datos_trabajador) => {
            con.query('SELECT * FROM tecnico',(err, datos_tecnico) => {
                con.query('SELECT * FROM equipo',(err, datos_equipo) => {
                    con.query('SELECT * FROM empresa',(err,datos_empresa) => {
                        con.query('SELECT * FROM mantenimiento WHERE ID_MANTENIMIENTO = ?',[id], (err, row) => {
                            res.render('administrador/bitacora/bitacora_upd',{
                                data: row[0], bdtrabajador: datos_trabajador, bdtecnico: datos_tecnico, bdequipo : datos_equipo, bdempresa : datos_empresa
                            });
                        });
                    });
                });
            });
        });
    });
};

controller.admbitacoraupd = (req, res) => {
    const { id } = req.params;
    const newdata = req.body;

    req.getConnection((err, con) => {
        con.query('UPDATE mantenimiento set ?  WHERE ID_MANTENIMIENTO = ?',[newdata,id], (err, row) => {
            res.redirect('/administrador/bitacora');
        })
    });
}

controller.pdf_bitacora = (req,res) =>{    
    req.getConnection((err, con) => {
        con.query('SELECT * FROM mantenimiento',(err, datos) => {
            if(err){
                res.json(err);
            }
            res.render('administrador/bitacora/pdf_bitacora',{data:datos})
        });    
    });
}

controller.admempresaadd = (req,res) => {
    const data = req.body;

    req.getConnection((err, con) => {
        con.query('INSERT INTO empresa set ?',[data], (err, row) => {
            console.log('empresa agregado');
            res.redirect('/administrador/empresa');
        })
    });
};

controller.admempresaupd = (req, res) => {
    const { id } = req.params;
    const newdata = req.body;

    req.getConnection((err, con) => {
        con.query('UPDATE empresa set ?  WHERE ID_EMPRESA = ?',[newdata,id], (err, row) => {
            res.redirect('/administrador/empresa');
        })
    });
}

controller.pdf_empresa = (req,res) =>{    
    req.getConnection((err, con) => {
        con.query('SELECT * FROM empresa',(err, datos) => {
            if(err){
                res.json(err);
            }
            res.render('administrador/empresa/pdf_empresa',{data:datos})
        });    
    });
}

controller.admequipoadd = (req,res) => {
    const data = req.body;

    req.getConnection((err, con) => {
        con.query('INSERT INTO equipo set ?',[data], (err, row) => {
            console.log('equipo agregado');
            res.redirect('/administrador/equipo');
        })
    });
};

controller.admequipodel = (req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('DELETE FROM equipo WHERE ID_EQUIPO = ?',[id], (err, row) => {
            console.log('equipo eliminado');
            res.redirect('/administrador/equipo');
        })
    });
};

controller.admequipoupd = (req, res) => {
    const { id } = req.params;
    const newdata = req.body;

    req.getConnection((err, con) => {
        con.query('UPDATE equipo set ?  WHERE ID_EQUIPO = ?',[newdata,id], (err, row) => {
            res.redirect('/administrador/equipo');
        })
    });
}

controller.pdf_equipo = (req,res) =>{    
    req.getConnection((err, con) => {
        con.query('SELECT * FROM equipo',(err, datos) => {
            if(err){
                res.json(err);
            }
            res.render('administrador/equipo/pdf_equipo',{data:datos})
        });    
    });
}

controller.admtrabajadoradd = (req,res) => {
    const data = req.body;
    req.getConnection((err, con) => {
        con.query('INSERT INTO trabajador set ?',[data], (err, row) => {
            console.log("trabajador agregado");
            res.redirect('/administrador/trabajador');
        })
    });
};

controller.admtrabajadorupd = (req, res) => {
    const { id } = req.params;
    const newdata = req.body;

    req.getConnection((err, con) => {
        con.query('UPDATE trabajador set ?  WHERE ID_TRABAJADOR = ?',[newdata,id], (err, row) => {
            res.redirect('/administrador/trabajador');
        })
    });
}

controller.pdf_trabajador = (req,res) =>{    
    req.getConnection((err, con) => {
        con.query('SELECT * FROM trabajador',(err, datos) => {
            if(err){
                res.json(err);
            }
            res.render('administrador/trabajador/pdf_trabajador',{data:datos})
        });    
    });
}

controller.admtecnicoadd = (req,res) => {
    const data = req.body;

    req.getConnection((err, con) => {
        con.query('INSERT INTO tecnico set ?',[data], (err, row) => {
            console.log('tecnico agregado');
            res.redirect('/administrador/tecnico');
        })
    });
};

controller.admtecnicoupd = (req, res) => {
    const { id } = req.params;
    const newdata = req.body;

    req.getConnection((err, con) => {
        con.query('UPDATE tecnico set ?  WHERE ID_TECNICO = ?',[newdata,id], (err, row) => {
            res.redirect('/administrador/tecnico');
        })
    });
}

controller.pdf_tecnico = (req,res) =>{    
    req.getConnection((err, con) => {
        con.query('SELECT * FROM tecnico INNER JOIN empresa on idempresa = ID_empresa',(err, datos) => {
            if(err){
                res.json(err);
            }
            res.render('administrador/tecnico/pdf_tecnico',{
                data: datos
            });
        });
    });
}

module.exports = controller;