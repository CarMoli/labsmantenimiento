const express = require('express')
//, routes = require('./routes')
, path = require('path')
, fileUpload = require('express-fileupload')
, app = express()
, mysql = require('mysql')
, bodyParser=require("body-parser");
const morgan = require('morgan');
const myConnection = require('express-myconnection');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const PassportLocal =require('passport-local').Strategy;
const { createConnection } = require('net');

app.use(fileUpload());

//static
app.use(express.static(path.join(__dirname,'public')));
app.use(express.static('public'));
app.use('/css', express.static(__dirname + 'public/css'));
app.use('/imagenes', express.static(__dirname + 'public/imagenes'));


//importando rutas
const rmain = require('./ruta/main')

//configuraciones
app.set('port',process.env.PORT || 3000 );
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//app.get('nueva.ejs', routes.index);//call for main index page
//app.post('/', routes.index);//call for signup post 


//middlewares
app.use(morgan('dev'));
/*
var connection = mysql.createConnection({
	host     : 'db4free.net',
	user     : 'bitacora',
    password : '0908.pass',
    port: 3306,
	database : 'servnano'
});

connection.connect();
 
global.db = connection;*/

app.use(myConnection(mysql,{
    host: 'db4free.net',
    user: 'bitacora',
    password: '0908.pass',
    port: 3306,
    database: 'servnano'
},'single'));

app.use(express.urlencoded({extended: false}));

//login
app.use(express.urlencoded({extended: true}));
app.set('view engine','ejs');
app.use(cookieParser('secreto'));
app.use(session({
    secret: 'secreto',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new PassportLocal(function(username,password,done){
    if(username==="admin" && password ==="12345")
        return done(null,{id: 1, name: "Admin"});
    done(null,false);
}));

passport.serializeUser(function(user,done){
    done(null,user.id);
});

passport.deserializeUser(function(id,done){
    done(null,{id: 1, name: "Admin"});
});

//rutas
app.use('/',rmain);
app.get("/administrador",(req,res) =>{
        res.render('administrador/administrador');
});

app.post("/administrador/login", passport.authenticate('local',{
    successRedirect:"/administrador/bitacora",
    failureRedirect:"/administrador"
})); 

app.get('/administrador/bitacora',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
}, (req,res) => {
    req.getConnection((err, con) => {
        con.query('SELECT * FROM `mantenimiento` INNER JOIN trabajador on idtrabajador = ID_trabajador INNER JOIN tecnico on idtecnico = Id_tecnico INNER JOIN equipo on idequipo = Id_equipo INNER JOIN empresa on idempresa = ID_empresa ORDER BY ID_MANTENIMIENTO',(err, datos) => {
            if(err){
                res.json(err);
            }
            res.render('administrador/bitacora/bitacora',{
                data: datos
            });
        });
    });
});

app.get('/administrador/empresa',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
}, (req,res) => {
    req.getConnection((err, con) => {
        con.query('SELECT * FROM empresa',(err, datos) => {
            if(err){
                res.json(err);
            }
            res.render('administrador/empresa/empresa',{
                data: datos
            });
        });
    });
});

app.get('/administrador/empresa/del/:id',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
}, (req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('DELETE FROM empresa WHERE ID_EMPRESA = ?',[id], (err, row) => {
            console.log('empresa eliminado');
            res.redirect('/administrador/empresa');
        })
    });
});

app.get('/administrador/empresa/update/:id',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
}, (req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('SELECT * FROM empresa WHERE ID_EMPRESA = ?',[id], (err, row) => {
            res.render('administrador/empresa/empresa_upd',{
                data: row[0]
            })
        })
    });
});

app.get('/administrador/equipo',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
},(req,res) => {
    req.getConnection((err, con) => {
        con.query('SELECT * FROM equipo',(err, datos) => {
            if(err){
                res.json(err);
            }
            res.render('administrador/equipo/equipo',{
                data: datos
            });
        });
    });
});

app.get('/administrador/equipo/del/:id',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
},(req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('DELETE FROM equipo WHERE ID_EQUIPO = ?',[id], (err, row) => {
            console.log('equipo eliminado');
            res.redirect('/administrador/equipo');
        })
    });
});

app.get('/administrador/equipo/update/:id',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
},(req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('SELECT * FROM equipo WHERE ID_EQUIPO = ?',[id], (err, row) => {
            res.render('administrador/equipo/equipo_upd',{
                data: row[0]
            })
        })
    });
});

app.get('/administrador/trabajador',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
},(req,res) => {
    req.getConnection((err, con) => {
        con.query('SELECT * FROM trabajador',(err, datos) => {
            if(err){
                res.json(err);
            }
            res.render('administrador/trabajador/trabajador',{
                data: datos
            });
        });
    });
});

app.get('/administrador/trabajador/del/:id',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
},(req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('DELETE FROM trabajador WHERE ID_TRABAJADOR = ?',[id], (err, row) => {
            console.log('trabajador eliminado');
            res.redirect('/administrador/trabajador');
        })
    });
});

app.get('/administrador/trabajador/update/:id',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
},(req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('SELECT * FROM trabajador WHERE ID_TRABAJADOR = ?',[id], (err, row) => {
            res.render('administrador/trabajador/trabajador_upd',{
                data: row[0]
            })
        })
    });
});

app.get('/administrador/tecnico',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
},(req,res) => {
    req.getConnection((err, con) => {
        con.query('SELECT * FROM tecnico INNER JOIN empresa on idempresa = ID_empresa',(err, datos) => {
            con.query('SELECT * FROM empresa',(err, empresa) => {
                if(err){
                    res.json(err);
                }
                res.render('administrador/tecnico/tecnico',{data:datos, bdempresa:empresa})
            });
        });
    });
});

app.get('/administrador/tecnico/del/:id',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
},(req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('DELETE FROM tecnico WHERE ID_TECNICO = ?',[id], (err, row) => {
            console.log('tecnico eliminado');
            res.redirect('/administrador/tecnico');
        })
    });
});

app.get('/administrador/tecnico/update/:id',(req,res,next) =>{
    if(req.isAuthenticated()) return next();
    res.redirect("/administrador");
},(req,res) => {
    const { id } = req.params;

    req.getConnection((err, con) => {
        con.query('SELECT * FROM tecnico WHERE ID_TECNICO = ?',[id], (err, row) => {
            con.query('SELECT * FROM empresa',(err, empresa) => {
                res.render('administrador/tecnico/tecnico_upd',{
                    data: row[0], bdempresa: empresa
                });
            });
        });
    });
});



//estaticos
app.use(express.static(path.join(__dirname,'public')));

app.listen(app.get('port'), () => {
    console.log('Servidor on');
});